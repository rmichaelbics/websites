
$(document).ready(function() {
  //var $menu = $(".menu-nav");
  var $menu_a = $(".menu-nav a");
  var id = false;
  var sections = [];
  var hash = function(h) {
    if (history.pushState) {
      history.pushState(null, null, h);
    } else {
      location.hash = h;
    }
  };

  $menu_a.click(function(event) {
    event.preventDefault();
    $("html, body").animate(
      {
        scrollTop: $($(this).attr("href")).offset().top-100
      },
      {
        duration: 700,
        complete: hash($(this).attr("href"))
      }
    );
      $(this).parent().siblings().find('a').removeClass('active');
      $(this).addClass('active');
  });

  $menu_a.each(function() {
    sections.push($($(this).attr("href")));
  });

  $(window).scroll(function(event) {
    var scrolling = $(this).scrollTop() + $(this).height() / 3;
    var scroll_id;
    for (var i in sections) {
      var section = sections[i];
      if (scrolling > section.offset().top) {
        scroll_id = section.attr("id");
      }
    }
    if (scroll_id !== id) {
      id = scroll_id;
      $menu_a.removeClass("active");
      $("a[href='#" + id + "']").addClass("active");
    }
  });
});




