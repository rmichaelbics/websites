
    $(document).ready(function(){

      $(window).scroll(function () {
        var sc = $(window).scrollTop()
        if (sc > 0) {
          $("#header-scroll").addClass("small")
        } else {
          $("#header-scroll").removeClass("small")
        }
      });

      $("#show-sear").click(function(){
        $(".form-inline").slideToggle("500");
      });

      $('.your-class').slick({
        dots: true,
        arrows:false,
        infinite: true,
        autoplay:true,
        speed: 300,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
      });

      new WOW().init();

    });

  $(document).ready(function(){

    // Multilevel dropdown script

    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
      if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
      }
      var $subMenu = $(this).next(".dropdown-menu");
      $subMenu.toggleClass('show');

      $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
        $('.dropdown-submenu .show').removeClass("show");
      });
      return false;
    });


    // topbar script

    $('#topbar-id li').click(function(){
      var item = $(this).attr("class");
      var main_class = $('.home-bg').attr("class")

      if($('.home-bg').hasClass(item)){
          $('.home-bg').removeAttr('class');  
      }

      else{
        $('.home-bg').attr('class', 'home-bg ' + item);
        $(this).addClass('pointer').siblings().removeClass('pointer');
      }
    });

     $('#top-nav li.nav-item').click(function(){
        $(this).addClass('active').siblings().removeClass('active');
    });



    $('#header-id li').click(function(e){

      var item = $(this).attr("class");
      var main_class = $('.home-bg').attr("class")

      if($('.home-bg').hasClass(item)){
          $('.home-bg').removeAttr('class');  
      }
      else{
        $('.home-bg').attr('class', 'home-bg ' + item);
        $(this).addClass('pointer').siblings().removeClass('pointer');
        e.stopPropagation();  
      }
    });


    //image backgreound change


    $('#img-sec li').click(function(e){

      var item = $(this).attr("class");
      var main_class = $('.home-bg').attr("class")

      if($('.home-bg').hasClass(item)){
          $('.home-bg').removeAttr('class');  
      }
      else{
        $('.home-bg').attr('class', 'home-bg ' + item);
        $(this).addClass('pointer').siblings().removeClass('pointer');
        e.stopPropagation();  
      }
    });


  // main menu 

      $('#main-top-nav li').click(function(e){

      var item = $(this).attr("class");
      var main_class = $('.home-bg').attr("class")

      if($('.home-bg').hasClass(item)){
          $('.home-bg').removeAttr('class');  
      }
      else{
        $('.home-bg').attr('class', 'home-bg ' + item);
        $(this).addClass('pointer').siblings().removeClass('pointer');
        e.stopPropagation();  
      }
    });

   // animate css 

   $('#img-sec li.anim-ate').click(function(){
        $('.anim-ate .home-section .head-one').removeClass('slideInLeft');
          $('.anim-ate .home-section .head-two ').removeClass('slideInRight');


        $('.anim-ate .home-section .head-one').css('animation-name', 'slideInUp');
        $('.anim-ate .home-section .head-two ').css('animation-name', 'slideInDown');

   });


   //footer background change

     $('#foot-sec li').click(function(e){

      var item = $(this).attr("class");
      var main_class = $('.footer-main').attr("class")

      if($('.footer-main').hasClass(item)){
          $('.footer-main').removeAttr('class');  
      }
      else{
        $('.footer-main').attr('class', 'footer-main ' + item);
        $(this).addClass('pointer').siblings().removeClass('pointer');
        e.stopPropagation();  
      }
    });

    $(".overlay-menu-li").click(function(){
    $(".default-navbar").hide();
    $(".menu-overlay").show();
    });

    $(".overlay-close").click(function(){
    $(".menu-overlay").hide();
    $(".default-navbar").show();
    });



    $(".slide-menu-li").click(function(){
    $(".nav-item").slideToggle();
    });


  });

