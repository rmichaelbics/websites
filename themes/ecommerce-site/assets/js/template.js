
        $(document).ready(function(){

        new WOW().init();

        // cart show js
        $(".cart-rel").click(function(){
          $(".cart-detail").toggleClass("cart-show");
        });

        $(".close-cart").click(function(){
          $(".cart-detail").removeClass("cart-show");
        });


        //header slider
        $('.slider-class').slick({  
        dots: false,
        infinite: true,
        speed: 300,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 567,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots:false,
            arrows:false
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
        });


        // popular product slider
        $('.popu-slide').slick({  
        dots: false,
        infinite: true,
        speed: 300,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '50px',
            slidesToShow: 1                
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
        });


        // about unescape

        $('.slider-people').slick({  
        dots: false,
        infinite: true,
        speed: 300,
        arrows:false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        });

        
        // instagram feed slider
        $('.your-class-two').slick({
        arrows:false,
        autoplay:true,
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '70px',
            slidesToShow: 1    
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
        });


        //product detail slider 
        $('.slider-for').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: true,
          asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          asNavFor: '.slider-for',
          dots: false,
          centerMode: true,
          focusOnSelect: true
        });

        });


        // header scroll js

        $(window).scroll(function () {
             var sc = $(window).scrollTop()
            if (sc > 0) {
                $("#header-scroll").addClass("small")
            } else {
                $("#header-scroll").removeClass("small")
            }
        });

        $(window).scroll(function() {
            var height = $(window).scrollTop();
            if (height > 100) {
                $('#myBtn').fadeIn();
            } else {
                $('#myBtn').fadeOut();
            }
        });
            
        $("#myBtn").click(function(event) {
          event.preventDefault();
          $("html, body").animate({ scrollTop: 0 }, "slow");
          return false;
        });
        

        // view more button js
        $(".more-btn").click(function(){
          $(".more-product").addClass("view-product");
          $(".view-more-btn").addClass("display-one");
        });

        // range slider js
        $( "#mySlider" ).slider({
          range: true,
          min: 10,
          max: 999,
          values: [ 10, 500 ],
          slide: function( event, ui ) {
          $( "#price" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
         }
        });
          
        $( "#price" ).val( "$" + $( "#mySlider" ).slider( "values", 0 ) +
               " - $" + $( "#mySlider" ).slider( "values", 1 ) );

        
        // grid and menu js
        $(".grid-icon").click(function(){
            $(".menu-view").hide();
            $(".grid-view").show();
            $(".mini-icon").addClass("grid-color");
            $(".menu-icon").removeClass("grid-color");
          });
        $(".menu-icon").click(function(){
            $(".grid-view").hide();
            $(".menu-view").show();
            $(".mini-icon").addClass("grid-color");
            $(".grid-icon").removeClass("grid-color");
         });

        // search menu js
        function openSearch() {
          document.getElementById("myOverlay").style.display = "block";
        }

        function closeSearch() {
          document.getElementById("myOverlay").style.display = "none";
        }

        // add and remove product js
        $('.add').click(function () {
            if ($(this).prev().val() < 100) {
              $(this).prev().val(+$(this).prev().val() + 1);
            }
        });
        $('.sub').click(function () {
            if ($(this).next().val() > 1) {
              if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
            }
        });





        







          